import './bootstrap'

import App from './app/Main.vue'
import router from './router'

Vue.http = Vue.prototype.$http = axios.create({
    baseURL: `${window.routeBase}/api`,
    headers: {
        'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content
    }
})

import Loader from './app/components/shared/Loader.vue'

Vue.component('loader', Loader)

new Vue({
    router,
    data(){
      return {
          player: {
              'name': 'asdfasd'
          }
      }
    },
    render: h => h(App)
}).$mount('#app')