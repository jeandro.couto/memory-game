
import Game from './../app/components/Game.vue'
import Ranking from './../app/components/Ranking.vue'
import Start from './../app/components/Start.vue'

const app = [
    { path: '/start', component: Start, name: 'start'},
    { path: '/jogar', component: Game, name: 'game'},
    { path: '/rankings', component: Ranking, name: 'ranking'},
]

const root = [
    { path: '/', redirect: '/start'}
]

export default [ ...root, ...app ]