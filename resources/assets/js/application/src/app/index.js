import Game from './components/Game.vue'
import Ranking from './components/Ranking.vue'
import Register from './components/Register.vue'

export default [
    { path: '/cadastro', component: Register, name: 'cadastro'},
    { path: '/jogar', component: Game, name: 'jogar'},
    { path: '/ranking', component: Ranking, name: 'ranking'},
]