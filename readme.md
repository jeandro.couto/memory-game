# Memory Game

> Requirements

- PHP 7.0 < 
- Composer

> Usage

```
$ git clone https://gitlab.com/jeandro.couto/memory-game.git

$ cp .env.exemple .env

edit .env 

$ composer install
$ php artisan:migrate

# Install npm dependencies
$ yarn install  or npm install
$ yarn run development or npm run development

$ php artisan:serve

```
## Demo
http://memory-game.jrcouto.com.br/
