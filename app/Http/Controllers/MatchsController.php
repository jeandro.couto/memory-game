<?php

namespace App\Http\Controllers;

use App\Models\Match;
use Illuminate\Http\Request;

class MatchsController extends Controller {


    public function index()
    {
        $matchs = Match::orderBy('rounds', 'asc')->get();

        return response()->json($matchs, 200);
    }

    public function store(Request $request)
    {
       $match = Match::create($request->all());

       return response()->json($match, 200);
    }
}