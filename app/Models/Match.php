<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    public $table = "matchs";

    public $fillable = [
        'player_name',
        'time',
        'rounds'
    ];
}
