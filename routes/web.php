<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');


Route::group(['prefix' => 'api'], function () {
    Route::get('match', 'MatchsController@index')->name('match.index');
    Route::post('match', 'MatchsController@store')->name('match.store');
});